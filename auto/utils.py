from django.db.migrations.operations.fields import AlterField as BaseAlterField


class AlterField(BaseAlterField):
    """Operation to change field of model from another app."""

    def __init__(self, *args, **kwargs):
        self._app_label = kwargs.pop('app_label', None)

        super().__init__(*args, **kwargs)

    def state_forwards(self, app_label, state):
        super().state_forwards(self._app_label or app_label, state)

    def database_forwards(
        self, app_label, schema_editor, from_state, to_state
    ):
        super().database_forwards(
            self._app_label or app_label,
            schema_editor,
            from_state,
            to_state,
        )

    def database_backwards(
        self, app_label, schema_editor, from_state, to_state
    ):
        super().database_backwards(
            self._app_label or app_label,
            schema_editor,
            from_state,
            to_state,
        )
