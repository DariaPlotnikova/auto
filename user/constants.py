from django.db.models import IntegerChoices


class Role(IntegerChoices):
    SYSTEM = 1, 'System administrator'
    CUSTOMER = 2, 'Customer'
    ADMINISTRATOR = 3, 'Administrator'
    PROVIDER = 4, 'Details & luquids provider'
    REPAIRMAN = 5, 'Repairman'
