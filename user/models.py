from django.contrib.auth.models import AbstractUser
from django.db import models

from .constants import Role


class User(AbstractUser):
    email = models.EmailField(
        'Email',
        unique=True,
    )
    username = models.CharField(
        'Username',
        max_length=150,
    )
    role = models.PositiveSmallIntegerField(
        'User role',
        choices=Role.choices,
        default=Role.CUSTOMER,
    )
    phone = models.CharField(
        'Phone',
        max_length=20,
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['role', 'username']
