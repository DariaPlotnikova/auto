from django.db import models

from .constants import RepairOperation
from .constants import Specialization
from .constants import Status


class Car(models.Model):

    owner = models.ForeignKey(
        'user.User',
        verbose_name='Owner',
        on_delete=models.CASCADE,
        related_name='cars',
    )
    brand = models.CharField(
        'Brand',
        max_length=50,
    )
    series = models.CharField(
        'Series',
        max_length=50,
    )
    vin = models.CharField(
        'VIN',
        max_length=20,
    )

    def __str__(self):
        return f'{self.brand} {self.series} ({self.owner})'


class Repairman(models.Model):

    user = models.ForeignKey(
        'user.User',
        on_delete=models.CASCADE,
    )
    specialization = models.PositiveSmallIntegerField(
        'Specialization',
        choices=Specialization.choices,
    )

    def __str__(self):
        return f'{self.get_specialization_display()} {self.user.email}: {self.user.phone}'


class Repair(models.Model):

    car = models.ForeignKey(
        Car,
        verbose_name='Car to repair',
        on_delete=models.RESTRICT,
        related_name='repairs',
    )
    executor = models.ForeignKey(
        Repairman,
        verbose_name='Executor',
        on_delete=models.RESTRICT,
        related_name='repairs',
    )
    started = models.DateTimeField(
        'Repair started at',
    )
    finished = models.DateTimeField(
        'Repair finished at',
        null=True, blank=True,
    )
    status = models.PositiveSmallIntegerField(
        'Status',
        choices=Status.choices,
        default=Status.PLANNED,
    )

    def __str__(self):
        return f'{self.executor.get_specialization_display()} | {self.car.brand} {self.car.series}'


class Operation(models.Model):

    repair = models.ForeignKey(
        Repair,
        verbose_name='Repair in which operation proceed',
        on_delete=models.CASCADE,
        related_name='operations',
    )
    operation = models.PositiveSmallIntegerField(
        'Operation to do',
        choices=RepairOperation.choices,
    )
    is_done = models.BooleanField(
        'Is done',
        default=False,
    )
