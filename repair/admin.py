from django.contrib import admin

from store.models import Detail
from store.models import Liquid
from user.models import User

from .models import Car
from .models import Operation
from .models import Repair
from .models import Repairman


class OperationInline(admin.StackedInline):
    model = Operation


class DetailInline(admin.TabularInline):
    model = Detail


class LiquidInline(admin.StackedInline):
    model = Liquid


class CarInline(admin.TabularInline):
    model = Car


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    list_display = ('brand', 'series', 'vin', 'owner')
    list_filter = ('brand',)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'phone', 'role', 'is_active')
    list_filter = ('role',)
    inlines = (CarInline,)


@admin.register(Repair)
class RepairAdmin(admin.ModelAdmin):
    list_display = ('started', 'finished', 'status', 'executor')
    list_filter = ('status',)
    list_editable = ('status',)
    inlines = (OperationInline, DetailInline, LiquidInline)


@admin.register(Repairman)
class RepairmanAdmin(admin.ModelAdmin):
    list_display = ('user', 'specialization')
    list_filter = ('specialization',)
