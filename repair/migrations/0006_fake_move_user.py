# Generated by Django 3.2.10 on 2021-12-23 19:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        # Freeze dependency to all apps which exist in project at
        # the moment. This helps to avoid problems with unpredictable
        # migrations order (sometimes django changes order of apps
        # which it applies migrations for).
        ('repair', '0005_fix_user_table_name'),
        ('store', '0003_move_detail_content_type'),
    ]

    operations = [
        # Migrations which will be created to move User mode must have exactly that names
        migrations.RunSQL('''
            INSERT INTO django_migrations (app, name, applied) 
            VALUES ('user', '0001_initial', datetime('now','localtime'));
            INSERT INTO django_migrations (app, name, applied) 
            VALUES ('user', '0002_move_user_content_type', datetime('now','localtime'));
            INSERT INTO django_migrations (app, name, applied)
            VALUES ('repair', '0007_move_user', datetime('now','localtime'));
        ''')
    ]
