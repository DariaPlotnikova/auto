from django.db.models.enums import IntegerChoices


class Specialization(IntegerChoices):
    ENGINE = 1, 'Engine'
    ELECTRICS = 2, 'Electrics'
    PENDANT = 3, 'Pendant'
    PAINT = 4, 'Paint and varnish surface'


class Status(IntegerChoices):
    PLANNED = 1, 'Planned'
    IN_PROGRESS = 2, 'In progress'
    WAIT_FOR_DETAILS = 3, 'Waiting for details and liquids'
    APPROVEMENT = 4, 'Wait for approve from customer'
    FINISHED = 5, 'Finished'


class RepairOperation(IntegerChoices):
    DIAGNOSTICS_ENGINE = 1, 'Engine diagnostics'
    DIAGNOSTICS_PENDANT = 2, 'Pendant diagnostics'
    CANDLES = 3, 'Change candles'
    NOZZLES = 4, 'Clean/change nozzles'
    TIRES = 5, 'Replace tires'
    GENERATOR = 6, 'Fix/replace generator'
    CHANGE_GLASS_MECHANISM = 7, 'Change glass lifting mechanism'
    CHANGE_STARTER_BRUSHE = 8, 'Change starter brushes'
    CHANGE_FUEL_FILTER = 9, 'Change fuel filter + liquid'
    CHANGE_GAS_DISTRIB_MECHANISM = 10, 'Change gas distribution mechanism'
    CHANGE_STABILIZER_BUSHING = 11, 'Change stabilizer bushing'
