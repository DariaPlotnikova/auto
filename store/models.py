from django.db import models

from repair.models import Repair

from .constants import OrderStatus


class Order(models.Model):
    repairman = models.ForeignKey(
        'repair.Repairman',
        verbose_name='Repairman who ordered details & liquids',
        on_delete=models.CASCADE,
        related_name='orders',
    )
    provider = models.ForeignKey(
        'user.User',
        verbose_name='Details provider',
        on_delete=models.RESTRICT,
        related_name='orders_to_provide',
    )
    customer = models.ForeignKey(
        'user.User',
        verbose_name='Customer',
        on_delete=models.RESTRICT,
        related_name='my_orders',
    )
    status = models.PositiveSmallIntegerField(
        'Order status',
        choices=OrderStatus.choices,
        default=OrderStatus.CREATED,
    )


class Detail(models.Model):

    repair = models.ForeignKey(
        Repair,
        verbose_name='Repair in which detail used',
        on_delete=models.CASCADE,
        related_name='details',
    )
    order = models.ForeignKey(
        'store.Order',
        verbose_name='Order in wich item was bought',
        null=True, blank=True,
        on_delete=models.RESTRICT,
        related_name='details',
    )
    description = models.CharField(
        'Which detail (for what to order)',
        max_length=255,
    )
    count = models.PositiveSmallIntegerField(
        'How many items to order',
    )


class Liquid(models.Model):

    repair = models.ForeignKey(
        Repair,
        verbose_name='Repair in which liquid used',
        on_delete=models.CASCADE,
        related_name='liquids',
    )
    order = models.ForeignKey(
        'store.Order',
        verbose_name='Order in wich item was bought',
        null=True, blank=True,
        on_delete=models.RESTRICT,
        related_name='liquids',
    )
    description = models.CharField(
        'Which liquid to order',
        max_length=255,
    )
    tech_characteristic = models.CharField(
        'Additional technical characteristics of liquid',
        max_length=255,
    )
    volume = models.FloatField('Volume to order')
