from django.db.models.enums import IntegerChoices


class OrderStatus(IntegerChoices):
    CREATED = 1, 'Created'
    ORDERED = 2, 'Ordered'
    DELIVERED = 3, 'Delivered'
