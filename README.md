## Car service

This Django project is example of how to separate models to several apps without data loss. 

It considers such cases:
* moving any domain-specific model (demonstrating [topic](https://docs.djangoproject.com/en/4.0/ref/migration-operations/#separatedatabaseandstate) from Django documentation);
* moving custom `User` model (which is tricky because `User` may be used by third-party
libraries and especially it is quite untrivial if you deploy automatically by means of such 
an instrument like Kubernetes).

## For whom

This example may be usefull for developers who what to prepare their monolith Django project to 
move to microservices by plan: "Firstly separate code base to low coupled parts, probably 
moving some models to another apps. Then move that separated code blocks to dedicated services".

## How to deal with that example

The main branch is `master` it is final state of project where all models have been already 
moved to appropriate Django applications. History of `master` contains all steps which leaded 
to final state, 1 step - N commits. Also on the first commit there is fixture which you can use 
to initialize your database with initial data (it changes appropriately in further commits, so you
can reuse it at any history point).

## Try it out
Default admin account: admin@example.com / q123123q.

To play with separating process step by step do following:

1. Clone repository
2. Go inside it
3. Move to commit with message starting with phrase `Step #1`
4. Install initial data from fixture
5. Run server, look at initial state of models, how they are located in applications, how admin
site looks like, look at how they should be located at the end (see picture below).
6. Go through commits starting with phrase `Step #...`, after each switching you need to apply
migrations to see how database tables are changing during the separation process.

## Commits explanation

1. Some commits have failed migrations state, they are marked with word `FAIL` in commit message.
They just demonstrate that obvious manner to move modes doesn't work. You shouldn't repeat that
step in your projects.
2. Step #1: in that step we just create typical django app (`repair`) with several models and 
after that add another one model. Nothing special.
3. Step #2: here we got new bussiness requirements which push us to situation where we need to 
split our django app on 2 (`repair` and `store`) and move 2 models (`Detail` and `Liquid`) from 
old app to new one. At the begining of that step we create model Order wich will `Detail` and 
`Liquid` will be related to.
4. Step #3: new bussiness requirement bring us a lot of features with User but they are 
absolutely unrelated to repair process. So it's time to create separate user app which will 
contain everything user-related.
There are 2 special commits applying migrations described in whose you must firstly stay on 
commit `Step #3 Faking further migrations ...` when it had succeed you can go on the next 
commit `Step #3 User model moved ...` and apply its migrations. If you don't do so errors in 
migrations graph will occure.

## Questions

If you have any misunderstanding or questions, feel free to contact me in 
Telegram `@plotnikova1111`.
